%SUB
%uses fplot and subplots.
%subplot(mnp) or subplot(m,n,p) splits the figure window into an m-by-n
%array of regions each having its own axes. The current plotting 
%command will then apply to the pth of these regions.
%example: subplot(425) splits the figure window into a 4-by-2 matrix 
%regions and specifies that plotting commands apply to the fifth region

%Use fplot to plot mathematical functions. It adaptively samples a 
%function at enough points to produce a representative graph

subplot(221), fplot(@(x) exp(sqrt(x)*sin(12*x)), [0 2*pi]);
subplot(222), fplot(@(x) sin(round(x)), [0 10], '--');
subplot(223), fplot(@(x) x,[-5 5], '-.');