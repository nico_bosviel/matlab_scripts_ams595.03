%ROULDIST
%Empirical distribution of number of real eigenvalues.

k = 1000;
tol = 1e-4;
wheel = zeros(k,1);
for i = 1:k
    A = randn(8);
    %Count number of eigenvalues with imaginary part < tol.
    wheel(i) = sum(abs(imag(eig(A))) < tol);
end
histogram(wheel,[-1 1 3 5 7 9]);