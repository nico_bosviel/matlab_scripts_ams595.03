%TRAPLOOP
%integrate exp(x)^(1/pi) over [0 1] using the composite trapezoidal rule on
%a uniform mesh with n subintervals and use a loop to do the sum

format long; %prints out the unformatted numbers to 12 digits

n = input('type n: '); %input n from the keyboard

h = 1/n; %uniform mesh

t = cputime;

integral = (h/2)*(exp(0)^(-1/pi) + exp(1)^(-1/pi)); %first and last terms 
                                                    %summed manually

for j=1:n-1
    integral = integral + h*(exp(j*h)^(-1/pi));
end

fprintf('\n CPUtime taken: %16.6g: \n',cputime-t);
fprintf('\n Approximate integral: %16.12f\n',integral);