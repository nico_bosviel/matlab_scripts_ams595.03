function max = maxentry(A)
%MAXENTRY   Largest absolute value of matrix entries
%           MAXENTRY(A) is the maximum of the absolute values
%           of the entries of A.
%Inputs: 
%  A-Matrix
%Output: 
%  max-maximum of the absolute values of the entries of A
%
%Other m-files required: none
%Subfunctions: none
%
%Author: Nicolas
%email: nicolas.bosviel@stonybrook.edu
%date: Sept 2017; Last revision: Sept 12th 2017

%----------------- BEGIN CODE ------------------

max = max(max(abs(A)));

%----------------- END CODE --------------------