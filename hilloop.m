%HILLOOP
%assemble the nxn Hilbert matrix using double for loop

n = input('type n: ');

Hloop = zeros(n,n);

t = cputime;

for i = 1:n
    for j = 1:n
        Hloop(i,j) = 1/(i+j-1);
    end
end

fprintf('\n time for assembling H: %16.8g\n',cputime-t);
