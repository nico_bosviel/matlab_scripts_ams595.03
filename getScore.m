function result = getScore(scoreVec)
%GETSCORE     Determines the average value of a vector of score
%             results.
%
%
%Inputs: 
%  SCOREVEC-Vector of scores
%Output: 
%  RESULT-average score result
%
%Other m-files required: none
%Subfunctions: none
%
%Author: Nicolas
%email: 
%date: Sept 2017; Last revision: Sept 12th 2017

%----------------- BEGIN CODE ------------------

result = mean(scoreVec); %Just a wrapper that does 
                         %the same thing as 'mean'

%----------------- END CODE --------------------
