function f = bell(x,a,b,c)
%BELL   Calculates the bell-shaped function 
%       f = a*exp(-(x-b)^2/(2*c*c))
%
%
%Inputs: 
%  X-value where the function is to be evaluated
%  A-parameter
%  B-expected value
%  C-standard deviation
%Output: 
%  F-scalar value
%
%Other m-files required: none
%Subfunctions: none
%
%Author: Nicolas
%email: 
%date: Sept 2017; Last revision: Sept 12th 2017

%----------------- BEGIN CODE ------------------

f = a*exp(-(x-b)^2/(2*c*c));

%----------------- END CODE --------------------