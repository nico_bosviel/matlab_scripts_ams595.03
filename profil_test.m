%PROFIL_TEST
%sample code to see how to use the profiler

A = rand(100,100);

h = waitbar(0,'please wait...');

for i=1:2000
    A = A.^2;
    waitbar(i/2000,h);
end

delete(h);