%PLOTLEGENDRE
%plots the Legendre polynomials of degree 1 to 4

x = -1:0.01:1;

p1 = x;
p2 = ((3/2)*x.^2 - 1/2);
p3 = (5/2)*x.^3 - (3/2)*x;
p4 = (35/8)*x.^4 - (15/4)*x.^2 + (3/8);

clf; %clears the current figure window

plot(x,p1,'r:',x,p2,'g--',x,p3,'b-.',x,p4,'m-');

P = [p1;p2;p3;p4];
                                
%plot(x,P);
                                
box off;

legend('n=1','n=2','n=3','n=4','Location','best');
xlabel('x','FontSize',12,'FontAngle','italic');
ylabel('P_n','Fontsize',12,'FontAngle','italic');
title('Legendre Polynomials','FontSize',14);
