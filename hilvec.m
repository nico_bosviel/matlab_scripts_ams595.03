%HILVEC
%assemble the nxn Hilbert matrix using vectorization 

n = input('type n: ');

Hvec = zeros(n,n);

t = cputime;

Hvec = ones(n,n)./((1:n)'*ones(1,n) + ones(n,1)*(1:n) - ones(1,1));

fprintf('\n time for assembling H: %16.8g\n',cputime-t);