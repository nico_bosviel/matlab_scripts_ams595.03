%EPICYCLOID
%plot x(t) = (a+b)*cos(t) - b*cos((a/b + 1)*t) 
%     y(t) = (a+b)*sin(t) - b*sin((a/b + 1)*t) 
%for 0 <= t <= 10*pi and a=12 and b=5

a = 12; b = 5;
t = 0:0.05:10*pi;
x = (a+b)*cos(t) - b*cos((a/b + 1)*t);
y = (a+b)*sin(t) - b*sin((a/b + 1)*t);

plot(x,y);
axis equal; %equalize data units on all axes
axis([-25 25 -25 25]);
grid on;

title('Epicycloid: a=12, b=5');
xlabel('x(t)');
ylabel('y(t)');