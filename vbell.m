function f = vbell(x,a,b,c)
%VBELL   Calculates the bell-shaped function 
%        f = a*exp(-(x-b)^2/(2*c*c)) for different values x
%        simultaneously.
%
%
%Inputs: 
%  X-vector of values where the function is to be evaluated
%  A-parameter
%  B-expected value
%  C-standard deviation
%Output: 
%  F-vector of the same nature as x
%
%Other m-files required: none
%Subfunctions: none
%
%Author: Nicolas
%email: 
%date: Sept 2017; Last revision: Sept 12th 2017

%----------------- BEGIN CODE ------------------

f = a*exp(-(x-b).^2/(2*c*c))

%----------------- END CODE --------------------
