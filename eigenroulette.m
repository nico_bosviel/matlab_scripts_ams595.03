%EIGENROULETTE
%Counts the number of real eigenvalues of a random matrix
%Note: this number can only be: 0, 2, 4, 6 or 8

A = randn(8);
tol = 1e-4;
imeig = imag(eig(A));
vec = abs(imeig) < tol;
disp(sum(vec));