%PLOT2
%animated plot showing the behaviour of the waves sin(x-t) and cos(x-t)
%for 201 equally spaced points in the range 0 <= x <= 8*pi
%and ranging from 0 to 10 in steps of 0.1

x = 8*pi*[0:200]/200; %201 equally spaced points between 0 and pi

for t = 0:100
    plot(x,sin(x-t),'bd:','MarkerSize',2); %plot sin wave
    
    axis tight; %set axis limit to range of data
    
    hold on;
    
    plot(x,cos(x-t), 'r--'); %plot cosine wave
    
    drawnow;  %is needed to ensure that animated plots are drawn
    
    pause(0.05);
    
    hold off;
    
end