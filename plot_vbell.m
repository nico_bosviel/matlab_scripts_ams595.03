%PLOT_VBELL 
%Uses the vbell function to plot a Gaussian curve

a = 1;
b = 0;
c = 1;
x = linspace(-10,10,1000);
plot(x,vbell(x,a,b,c),'b');