%TAYLORERR
%use a loglog plot to reveal power-law relationship as straight lines

h = 10.^[0:-1:-4];

taylerr = abs((1+h+h.^2/2)-exp(h));

clf;

 
loglog(h,taylerr,'-',h,h.^3,'--');

xlabel('h');
ylabel('abs(error)');
title('Error in quadratic Taylor series approx to exp(h)');
box off;
