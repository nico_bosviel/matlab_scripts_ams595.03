function result = getScorePurge(scoreVec)
%GETSCOREPURGE     Determines the average value of a vector 
%                  of score results stripped from the best two
%                  score.
%
%
%Inputs: 
%  SCOREVEC-Vector of scores
%Output: 
%  RESULT-average score result
%
%Other m-files required: none
%Subfunctions: purgeTop
%
%Author: Nicolas
%email: 
%date: Sept 2017; Last revision: Sept 12th 2017

%----------------- BEGIN CODE ------------------

scoreVec = purgeTop(scoreVec); %Purge the highest value
scoreVec = purgeTop(scoreVec); %Purge the 2nd highest value
result = mean(scoreVec);
                         
%----------------- END CODE --------------------
    
function vecOut = purgeTop(vecIn)
%PURGETOP     Removes the highest entry of a vector 
%             
%
%Inputs: 
%  VECIN-Vector
%Output: 
%  VECOUT-Vector
%
%Other m-files required: none
%Subfunctions: none
%
%Author: Nicolas
%email: 
%date: Sept 2017; Last revision: Sept 12th 2017
        
%----------------- BEGIN CODE ------------------

[top, vi] = max(vecIn); %top is the highest entry and 
                        %vi is a vector of indices 
vecOut = vecIn;
vecOut(vi(1)) = [];

%----------------- END CODE --------------------

