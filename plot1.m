%PLOT1
%Explore the use of some plotting commands

x = pi*([0:100]/100); %row vector of 101 equally spaced 
                      %points between 0 and pi
         
hold off; %the next plot will be drawn on a clean set of axes

plot(x,sin(x),'r*'); %plots sin(x) against x using red '*'

hold on; %the next plot will be drawn on top of the present one

plot(x,(x-x.^3/factorial(3)),'bo'); %plots values of x-(x^3)/3! against x

pause; %causes execution to stop until you next hit any key
       %useful for admiring intermediate plots

%second example draws a 3D helix composed of the points
%(sin(t),cos(t),t) for t ranging over 301 equally 
%spaced points between 0 and 100

t = [0:300]/3;

hold off;

plot3(sin(t),cos(t),t,'r*',sin(t),cos(t),t,'g-');

pause;

%now lets do an animated plot of this set of values
%using the command 'comet'

comet3(sin(t),cos(t),t);